﻿using Microsoft.EntityFrameworkCore;
using System;
using Entities;
namespace DataAccess
{
    public class D_COMM_DAL
    {
        public class SQLiteDBContext : DbContext
        {
            protected override void OnConfiguring(DbContextOptionsBuilder options)
                => options.UseSqlite("Data Source=C:\\ProgramData\\D_COMM\\dcommsqlite.db");
        }

        public int GetUser()
        {
            try
            {
                using (var db = new SQLiteDBContext())
                {
                    var cmd = db.Database.GetDbConnection().CreateCommand();
                    cmd.CommandText = string.Format("select USER_ID, ROLE from USERS where USER_NAME = '{0}' and PASSWORD = '{1}'",User.UserName, User.Password);
                    db.Database.OpenConnection();
                    var result = cmd.ExecuteReader();
                    while (result.Read())
                    {
                        User.UserId = Convert.ToInt32(result[0].ToString());
                        User.Role = result[1].ToString();
                        return User.UserId;
                    }
                }
            }
            catch (Exception)
            {
                return 0;
            }
            return 0;            
        }

        public bool InsertDenom(Denomination d)
        {
            try
            {
                using (var db = new SQLiteDBContext())
                {
                    var cmd = db.Database.GetDbConnection().CreateCommand();
                    cmd.CommandText = string.Format("insert into DENOMINATION values('{0}','{1}',{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},'{12}')", d.Date, d.User, d.Rs2000, d.Rs500, d.Rs200, d.Rs100, d.Rs50, d.Rs20, d.Rs10, d.Rs5, d.Rs2, d.Rs1, d.LastUpdateDate);
                    db.Database.OpenConnection();
                    cmd.ExecuteNonQuery();
                }
                return true;
            }
            catch
            {                
                return false;
            }
        }
        public bool UpdateDenom(Denomination d)
        {
            try
            {
                using (var db = new SQLiteDBContext())
                {
                    var cmd = db.Database.GetDbConnection().CreateCommand();
                    cmd.CommandText = string.Format("update DENOMINATION " +
                        "set ENTERED_BY = '{0}', RS_2000 = {1}, RS_500 = {2}, RS_200 = {3}, RS_100 = {4}, RS_50 = {5}, RS_20 = {6}, RS_10 = {7}, RS_5 = {8}, RS_2 = {9}, RS_1 = {10}, LAST_UPDATE_DATE = '{11}'" +
                        " where DATE = '{12}'"
                        , d.User, d.Rs2000, d.Rs500, d.Rs200, d.Rs100, d.Rs50, d.Rs20, d.Rs10, d.Rs5, d.Rs2, d.Rs1, d.LastUpdateDate, d.Date);
                    db.Database.OpenConnection();
                    cmd.ExecuteNonQuery();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool GetDenoms(Denomination d)
        {
            bool hasData = false;
            try
            {
                using (var db = new SQLiteDBContext())
                {
                    var cmd = db.Database.GetDbConnection().CreateCommand();
                    cmd.CommandText = string.Format("Select * from DENOMINATION where date = '{0}'", d.Date);
                    db.Database.OpenConnection();
                    var result = cmd.ExecuteReader();
                    while (result.Read())
                    {
                        hasData = true;
                        d.User = result[1].ToString();
                        d.Rs2000 = Convert.ToInt32(result[2].ToString());
                        d.Rs500 = Convert.ToInt32(result[3].ToString());
                        d.Rs200 = Convert.ToInt32(result[4].ToString());
                        d.Rs100 = Convert.ToInt32(result[5].ToString());
                        d.Rs50 = Convert.ToInt32(result[6].ToString());
                        d.Rs20 = Convert.ToInt32(result[7].ToString());
                        d.Rs10 = Convert.ToInt32(result[8].ToString());
                        d.Rs5 = Convert.ToInt32(result[9].ToString());
                        d.Rs2 = Convert.ToInt32(result[10].ToString());
                        d.Rs1 = Convert.ToInt32(result[11].ToString());
                        d.LastUpdateDate = result[12].ToString();
                    }
                }                
                return hasData;
            }
            catch
            {
                return hasData;
            }
        }
    }
}
