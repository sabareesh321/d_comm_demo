﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessLogic;
using Entities;

namespace D_COMM
{
    public partial class D_COMM_MAIN : Form
    {
        private bool isEdit = false;
        private readonly D_COMM_BLL bll = new D_COMM_BLL();
        public D_COMM_MAIN()
        {
            InitializeComponent();

        }
        private void C_COMM_MAIN_Load(object sender, EventArgs e)
        {
            Login login = new Login();
            login.ShowDialog();
        }
        private void C_COMM_MAIN_FormClosing(object sender, FormClosedEventArgs e)
        {

        }

        private void tc_main_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.AcceptButton = btn_dnm_submit;
        }

        private void GetDenomination()
        {
            CultureInfo provider = CultureInfo.InvariantCulture;
            Denomination denomination = new Denomination();
            denomination.Date = dtp_denominations.Value.ToString("yyyy-MM-dd");
            if (bll.GetDenoms(denomination))
            {
                tb_2000.Enabled = false;
                tb_500.Enabled = false;
                tb_200.Enabled = false;
                tb_100.Enabled = false;
                tb_50.Enabled = false;
                tb_20.Enabled = false;
                tb_10.Enabled = false;
                tb_5.Enabled = false;
                tb_2.Enabled = false;
                tb_1.Enabled = false;
                btn_dnm_edit.Enabled = true;

                tb_2000.Text = denomination.Rs2000.ToString();
                tb_500.Text = denomination.Rs500.ToString();
                tb_200.Text = denomination.Rs200.ToString();
                tb_100.Text = denomination.Rs100.ToString();
                tb_50.Text = denomination.Rs50.ToString();
                tb_20.Text = denomination.Rs20.ToString();
                tb_10.Text = denomination.Rs10.ToString();
                tb_5.Text = denomination.Rs5.ToString();
                tb_2.Text = denomination.Rs2.ToString();
                tb_1.Text = denomination.Rs1.ToString();
                lbl_lstUpdVal.Text = DateTime.ParseExact(denomination.LastUpdateDate, "yyyy-MM-dd HH:mm:ss", provider).ToString("dd-MMM-yyyy HH:mm:ss").ToUpper();
                lbl_UserValue.Text = denomination.User;
            }
            else
            {
                tb_2000.Enabled = true;
                tb_500.Enabled = true;
                tb_200.Enabled = true;
                tb_100.Enabled = true;
                tb_50.Enabled = true;
                tb_20.Enabled = true;
                tb_10.Enabled = true;
                tb_5.Enabled = true;
                tb_2.Enabled = true;
                tb_1.Enabled = true;
                btn_dnm_edit.Enabled = false;

                tb_2000.Text = "0";
                tb_500.Text = "0";
                tb_200.Text = "0";
                tb_100.Text = "0";
                tb_50.Text = "0";
                tb_20.Text = "0";
                tb_10.Text = "0";
                tb_5.Text = "0";
                tb_2.Text = "0";
                tb_1.Text = "0";
                lbl_lstUpdVal.Text = "01-JAN-1900 00:00:00";
                lbl_UserValue.Text = "Unknown";
                //MessageBox.Show("No Data Found!!");
            }
            lbl_Rs2000.Text = denomination.Total2000.ToString();
            lbl_Rs500.Text = denomination.Total500.ToString();
            lbl_Rs200.Text = denomination.Total200.ToString();
            lbl_Rs100.Text = denomination.Total100.ToString();
            lbl_Rs50.Text = denomination.Total50.ToString();
            lbl_Rs20.Text = denomination.Total20.ToString();
            lbl_Rs10.Text = denomination.Total10.ToString();
            lbl_Rs5.Text = denomination.Total5.ToString();
            lbl_Rs2.Text = denomination.Total2.ToString();
            lbl_Rs1.Text = denomination.Total1.ToString();
            lbl_RsTotal.Text = denomination.RsTotal.ToString();
            lbl_DateValue.Text = DateTime.ParseExact(denomination.Date, "yyyy-MM-dd", provider).ToString("dd-MMM-yyyy").ToUpper();
        }

        private void btn_dnm_submit_Click(object sender, EventArgs e)
        {
            Denomination denomination = new Denomination();
            denomination.Rs2000 = Convert.ToInt32(tb_2000.Text);
            denomination.Rs500 = Convert.ToInt32(tb_500.Text);
            denomination.Rs200 = Convert.ToInt32(tb_200.Text);
            denomination.Rs100 = Convert.ToInt32(tb_100.Text);
            denomination.Rs50 = Convert.ToInt32(tb_50.Text);
            denomination.Rs20 = Convert.ToInt32(tb_20.Text);
            denomination.Rs10 = Convert.ToInt32(tb_10.Text);
            denomination.Rs5 = Convert.ToInt32(tb_5.Text);
            denomination.Rs2 = Convert.ToInt32(tb_2.Text);
            denomination.Rs1 = Convert.ToInt32(tb_1.Text);
            denomination.Date = dtp_denominations.Value.ToString("yyyy-MM-dd");
            denomination.LastUpdateDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            denomination.User = User.UserName;
            if(isEdit)
            {
                bll.EditDenom(denomination);
                isEdit = false;
            }
            else
                bll.SubmitDenoms(denomination);
            GetDenomination();
        }

        private void dtp_denominations_ValueChanged(object sender, EventArgs e)
        {
            Denomination denomination = new Denomination();
            denomination.Date = dtp_denominations.Value.ToString("yyyy-MM-dd");
            if (bll.GetDenoms(denomination))
            {
                tb_2000.Text = denomination.Rs2000.ToString();
                tb_500.Text = denomination.Rs500.ToString();
                tb_200.Text = denomination.Rs200.ToString();
                tb_100.Text = denomination.Rs100.ToString();
                tb_50.Text = denomination.Rs50.ToString();
                tb_20.Text = denomination.Rs20.ToString();
                tb_10.Text = denomination.Rs10.ToString();
                tb_5.Text = denomination.Rs5.ToString();
                tb_2.Text = denomination.Rs2.ToString();
                tb_1.Text = denomination.Rs1.ToString();
            }
            else
            {
                tb_2000.Text = "0";
                tb_500.Text = "0";
                tb_200.Text = "0";
                tb_100.Text = "0";
                tb_50.Text = "0";
                tb_20.Text = "0";
                tb_10.Text = "0";
                tb_5.Text = "0";
                tb_2.Text = "0";
                tb_1.Text = "0";
                MessageBox.Show("No Data Found!!");
            }
        }

        private void btn_getDenom_Click(object sender, EventArgs e)
        {
            GetDenomination();
        }

        private void setTotal()
        {
            lbl_RsTotal.Text = (Convert.ToInt32(lbl_Rs2000.Text) + Convert.ToInt32(lbl_Rs500.Text)
                + Convert.ToInt32(lbl_Rs200.Text) + Convert.ToInt32(lbl_Rs100.Text)
                + Convert.ToInt32(lbl_Rs50.Text) + Convert.ToInt32(lbl_Rs20.Text)
                + Convert.ToInt32(lbl_Rs10.Text) + Convert.ToInt32(lbl_Rs5.Text)
                + Convert.ToInt32(lbl_Rs2.Text) + Convert.ToInt32(lbl_Rs1.Text)).ToString();
        }
        private void tb_2000_TextChanged(object sender, EventArgs e)
        {
            string temp = tb_2000.Text;
            if (string.IsNullOrEmpty(tb_2000.Text))
            {
                temp = "0";
            }
            lbl_Rs2000.Text = (Convert.ToInt32(temp) * 2000).ToString();
            setTotal();

        }

        private void tb_500_TextChanged(object sender, EventArgs e)
        {
            string temp = tb_500.Text;
            if (string.IsNullOrEmpty(tb_500.Text))
            {
                temp = "0";

            }
            lbl_Rs500.Text = (Convert.ToInt32(temp) * 500).ToString();
            setTotal();

        }

        private void tb_200_TextChanged(object sender, EventArgs e)
        {
            string temp = tb_200.Text;
            if (string.IsNullOrEmpty(tb_200.Text))
            {
                temp = "0";

            }
            lbl_Rs200.Text = (Convert.ToInt32(temp) * 200).ToString();
            setTotal();

        }

        private void tb_100_TextChanged(object sender, EventArgs e)
        {
            string temp = tb_100.Text;
            if (string.IsNullOrEmpty(tb_100.Text))
            {
                temp = "0";

            }
            lbl_Rs100.Text = (Convert.ToInt32(temp) * 100).ToString();
            setTotal();

        }

        private void tb_50_TextChanged(object sender, EventArgs e)
        {
            string temp = tb_50.Text;
            if (string.IsNullOrEmpty(tb_50.Text))
            {
                temp = "0";

            }
            lbl_Rs50.Text = (Convert.ToInt32(temp) * 50).ToString();
            setTotal();

        }

        private void tb_20_TextChanged(object sender, EventArgs e)
        {
            string temp = tb_20.Text;
            if (string.IsNullOrEmpty(tb_20.Text))
            {
                temp = "0";
            }
            lbl_Rs20.Text = (Convert.ToInt32(temp) * 20).ToString();
            setTotal();

        }

        private void tb_10_TextChanged(object sender, EventArgs e)
        {
            string temp = tb_10.Text;
            if (string.IsNullOrEmpty(tb_10.Text))
            {
                temp = "0";

            }
            lbl_Rs10.Text = (Convert.ToInt32(temp) * 10).ToString();
            setTotal();
        }

        private void tb_5_TextChanged(object sender, EventArgs e)
        {
            string temp = tb_5.Text;
            if (string.IsNullOrEmpty(tb_5.Text))
            {
                temp = "0";
            }
            lbl_Rs5.Text = (Convert.ToInt32(temp) * 5).ToString();
            setTotal();
        }

        private void tb_2_TextChanged(object sender, EventArgs e)
        {
            string temp = tb_2.Text;
            if (string.IsNullOrEmpty(tb_2.Text))
            {
                temp = "0";
            }
            lbl_Rs2.Text = (Convert.ToInt32(temp) * 2).ToString();
            setTotal();
        }

        private void tb_1_TextChanged(object sender, EventArgs e)
        {
            string temp = tb_1.Text;
            if (string.IsNullOrEmpty(tb_1.Text))
            {
                temp = "0";
            }
            lbl_Rs1.Text = (Convert.ToInt32(temp) * 1).ToString();
            setTotal();
        }

        private void btn_dnm_edit_Click(object sender, EventArgs e)
        {
            if (User.Role.Equals("admin"))
            {
                tb_2000.Enabled = true;
                tb_500.Enabled = true;
                tb_200.Enabled = true;
                tb_100.Enabled = true;
                tb_50.Enabled = true;
                tb_20.Enabled = true;
                tb_10.Enabled = true;
                tb_5.Enabled = true;
                tb_2.Enabled = true;
                tb_1.Enabled = true;
                btn_dnm_edit.Enabled = false;
                isEdit = true;
            }
            else
                MessageBox.Show("You are not Admin!!");
            
        }
    }
}
