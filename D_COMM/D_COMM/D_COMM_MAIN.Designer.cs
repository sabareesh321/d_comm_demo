﻿using BusinessLogic;
using System;
using System.Windows.Forms;

namespace D_COMM
{
    partial class D_COMM_MAIN
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tc_main = new System.Windows.Forms.TabControl();
            this.tp_summary = new System.Windows.Forms.TabPage();
            this.tp_denominations = new System.Windows.Forms.TabPage();
            this.lbl_lstUpdVal = new System.Windows.Forms.Label();
            this.lbl_UserValue = new System.Windows.Forms.Label();
            this.lbl_DateValue = new System.Windows.Forms.Label();
            this.lbl_lstUpd = new System.Windows.Forms.Label();
            this.lbl_User = new System.Windows.Forms.Label();
            this.lbl_Date = new System.Windows.Forms.Label();
            this.lbl_RsTotal = new System.Windows.Forms.Label();
            this.lbl_Total = new System.Windows.Forms.Label();
            this.btn_getDenom = new System.Windows.Forms.Button();
            this.btn_dnm_submit = new System.Windows.Forms.Button();
            this.btn_dnm_edit = new System.Windows.Forms.Button();
            this.dtp_denominations = new System.Windows.Forms.DateTimePicker();
            this.tlp_denominations = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_2000 = new System.Windows.Forms.Label();
            this.lbl_500 = new System.Windows.Forms.Label();
            this.lbl_200 = new System.Windows.Forms.Label();
            this.lbl_100 = new System.Windows.Forms.Label();
            this.lbl_50 = new System.Windows.Forms.Label();
            this.lbl_20 = new System.Windows.Forms.Label();
            this.lbl_10 = new System.Windows.Forms.Label();
            this.lbl_5x = new System.Windows.Forms.Label();
            this.lbl_2x = new System.Windows.Forms.Label();
            this.lbl_1 = new System.Windows.Forms.Label();
            this.tb_2000 = new System.Windows.Forms.TextBox();
            this.tb_500 = new System.Windows.Forms.TextBox();
            this.tb_200 = new System.Windows.Forms.TextBox();
            this.tb_100 = new System.Windows.Forms.TextBox();
            this.tb_50 = new System.Windows.Forms.TextBox();
            this.tb_20 = new System.Windows.Forms.TextBox();
            this.tb_10 = new System.Windows.Forms.TextBox();
            this.tb_5 = new System.Windows.Forms.TextBox();
            this.tb_2 = new System.Windows.Forms.TextBox();
            this.tb_1 = new System.Windows.Forms.TextBox();
            this.lbl_Rs2000 = new System.Windows.Forms.Label();
            this.lbl_Rs500 = new System.Windows.Forms.Label();
            this.lbl_Rs200 = new System.Windows.Forms.Label();
            this.lbl_Rs100 = new System.Windows.Forms.Label();
            this.lbl_Rs50 = new System.Windows.Forms.Label();
            this.lbl_Rs20 = new System.Windows.Forms.Label();
            this.lbl_Rs10 = new System.Windows.Forms.Label();
            this.lbl_Rs5 = new System.Windows.Forms.Label();
            this.lbl_Rs2 = new System.Windows.Forms.Label();
            this.lbl_Rs1 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tc_main.SuspendLayout();
            this.tp_denominations.SuspendLayout();
            this.tlp_denominations.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tc_main
            // 
            this.tc_main.Controls.Add(this.tp_summary);
            this.tc_main.Controls.Add(this.tp_denominations);
            this.tc_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tc_main.Location = new System.Drawing.Point(0, 0);
            this.tc_main.Name = "tc_main";
            this.tc_main.SelectedIndex = 0;
            this.tc_main.Size = new System.Drawing.Size(925, 523);
            this.tc_main.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.tc_main.TabIndex = 3;
            this.tc_main.SelectedIndexChanged += new System.EventHandler(this.tc_main_SelectedIndexChanged);
            // 
            // tp_summary
            // 
            this.tp_summary.Location = new System.Drawing.Point(4, 29);
            this.tp_summary.Name = "tp_summary";
            this.tp_summary.Padding = new System.Windows.Forms.Padding(3);
            this.tp_summary.Size = new System.Drawing.Size(917, 490);
            this.tp_summary.TabIndex = 0;
            this.tp_summary.Text = "Summary";
            this.tp_summary.UseVisualStyleBackColor = true;
            // 
            // tp_denominations
            // 
            this.tp_denominations.Controls.Add(this.lbl_lstUpdVal);
            this.tp_denominations.Controls.Add(this.lbl_UserValue);
            this.tp_denominations.Controls.Add(this.lbl_DateValue);
            this.tp_denominations.Controls.Add(this.lbl_lstUpd);
            this.tp_denominations.Controls.Add(this.lbl_User);
            this.tp_denominations.Controls.Add(this.lbl_Date);
            this.tp_denominations.Controls.Add(this.lbl_RsTotal);
            this.tp_denominations.Controls.Add(this.lbl_Total);
            this.tp_denominations.Controls.Add(this.btn_getDenom);
            this.tp_denominations.Controls.Add(this.btn_dnm_submit);
            this.tp_denominations.Controls.Add(this.btn_dnm_edit);
            this.tp_denominations.Controls.Add(this.dtp_denominations);
            this.tp_denominations.Controls.Add(this.tlp_denominations);
            this.tp_denominations.Location = new System.Drawing.Point(4, 29);
            this.tp_denominations.Name = "tp_denominations";
            this.tp_denominations.Padding = new System.Windows.Forms.Padding(3);
            this.tp_denominations.Size = new System.Drawing.Size(917, 490);
            this.tp_denominations.TabIndex = 1;
            this.tp_denominations.Text = "Denominations";
            this.tp_denominations.UseVisualStyleBackColor = true;
            // 
            // lbl_lstUpdVal
            // 
            this.lbl_lstUpdVal.AutoSize = true;
            this.lbl_lstUpdVal.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_lstUpdVal.Location = new System.Drawing.Point(129, 144);
            this.lbl_lstUpdVal.Name = "lbl_lstUpdVal";
            this.lbl_lstUpdVal.Size = new System.Drawing.Size(171, 20);
            this.lbl_lstUpdVal.TabIndex = 11;
            this.lbl_lstUpdVal.Text = "01-JAN-1900 00:00:00";
            // 
            // lbl_UserValue
            // 
            this.lbl_UserValue.AutoSize = true;
            this.lbl_UserValue.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_UserValue.Location = new System.Drawing.Point(129, 112);
            this.lbl_UserValue.Name = "lbl_UserValue";
            this.lbl_UserValue.Size = new System.Drawing.Size(76, 20);
            this.lbl_UserValue.TabIndex = 10;
            this.lbl_UserValue.Text = "Unknown";
            // 
            // lbl_DateValue
            // 
            this.lbl_DateValue.AutoSize = true;
            this.lbl_DateValue.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_DateValue.Location = new System.Drawing.Point(129, 80);
            this.lbl_DateValue.Name = "lbl_DateValue";
            this.lbl_DateValue.Size = new System.Drawing.Size(105, 20);
            this.lbl_DateValue.TabIndex = 9;
            this.lbl_DateValue.Text = "01-JAN-1900";
            // 
            // lbl_lstUpd
            // 
            this.lbl_lstUpd.AutoSize = true;
            this.lbl_lstUpd.Location = new System.Drawing.Point(23, 144);
            this.lbl_lstUpd.Name = "lbl_lstUpd";
            this.lbl_lstUpd.Size = new System.Drawing.Size(92, 20);
            this.lbl_lstUpd.TabIndex = 8;
            this.lbl_lstUpd.Text = "Last Updted:";
            // 
            // lbl_User
            // 
            this.lbl_User.AutoSize = true;
            this.lbl_User.Location = new System.Drawing.Point(23, 112);
            this.lbl_User.Name = "lbl_User";
            this.lbl_User.Size = new System.Drawing.Size(41, 20);
            this.lbl_User.TabIndex = 7;
            this.lbl_User.Text = "User:";
            // 
            // lbl_Date
            // 
            this.lbl_Date.AutoSize = true;
            this.lbl_Date.Location = new System.Drawing.Point(23, 80);
            this.lbl_Date.Name = "lbl_Date";
            this.lbl_Date.Size = new System.Drawing.Size(44, 20);
            this.lbl_Date.TabIndex = 7;
            this.lbl_Date.Text = "Date:";
            // 
            // lbl_RsTotal
            // 
            this.lbl_RsTotal.AutoSize = true;
            this.lbl_RsTotal.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_RsTotal.Location = new System.Drawing.Point(614, 385);
            this.lbl_RsTotal.Name = "lbl_RsTotal";
            this.lbl_RsTotal.Size = new System.Drawing.Size(18, 20);
            this.lbl_RsTotal.TabIndex = 6;
            this.lbl_RsTotal.Text = "0";
            // 
            // lbl_Total
            // 
            this.lbl_Total.AutoSize = true;
            this.lbl_Total.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_Total.Location = new System.Drawing.Point(560, 385);
            this.lbl_Total.Name = "lbl_Total";
            this.lbl_Total.Size = new System.Drawing.Size(48, 20);
            this.lbl_Total.TabIndex = 5;
            this.lbl_Total.Text = "Total:";
            // 
            // btn_getDenom
            // 
            this.btn_getDenom.Location = new System.Drawing.Point(286, 12);
            this.btn_getDenom.Name = "btn_getDenom";
            this.btn_getDenom.Size = new System.Drawing.Size(94, 29);
            this.btn_getDenom.TabIndex = 4;
            this.btn_getDenom.Text = "Get Denom";
            this.btn_getDenom.UseVisualStyleBackColor = true;
            this.btn_getDenom.Click += new System.EventHandler(this.btn_getDenom_Click);
            // 
            // btn_dnm_submit
            // 
            this.btn_dnm_submit.Location = new System.Drawing.Point(617, 429);
            this.btn_dnm_submit.Name = "btn_dnm_submit";
            this.btn_dnm_submit.Size = new System.Drawing.Size(94, 29);
            this.btn_dnm_submit.TabIndex = 3;
            this.btn_dnm_submit.Text = "Submit";
            this.btn_dnm_submit.UseVisualStyleBackColor = true;
            this.btn_dnm_submit.Click += new System.EventHandler(this.btn_dnm_submit_Click);
            // 
            // btn_dnm_edit
            // 
            this.btn_dnm_edit.Enabled = false;
            this.btn_dnm_edit.Location = new System.Drawing.Point(483, 429);
            this.btn_dnm_edit.Name = "btn_dnm_edit";
            this.btn_dnm_edit.Size = new System.Drawing.Size(94, 29);
            this.btn_dnm_edit.TabIndex = 2;
            this.btn_dnm_edit.Text = "Edit";
            this.btn_dnm_edit.UseVisualStyleBackColor = true;
            this.btn_dnm_edit.Click += new System.EventHandler(this.btn_dnm_edit_Click);
            // 
            // dtp_denominations
            // 
            this.dtp_denominations.Location = new System.Drawing.Point(10, 12);
            this.dtp_denominations.Name = "dtp_denominations";
            this.dtp_denominations.Size = new System.Drawing.Size(250, 27);
            this.dtp_denominations.TabIndex = 1;
            // 
            // tlp_denominations
            // 
            this.tlp_denominations.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tlp_denominations.ColumnCount = 3;
            this.tlp_denominations.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlp_denominations.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlp_denominations.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlp_denominations.Controls.Add(this.lbl_2000, 0, 0);
            this.tlp_denominations.Controls.Add(this.lbl_500, 0, 1);
            this.tlp_denominations.Controls.Add(this.lbl_200, 0, 2);
            this.tlp_denominations.Controls.Add(this.lbl_100, 0, 3);
            this.tlp_denominations.Controls.Add(this.lbl_50, 0, 4);
            this.tlp_denominations.Controls.Add(this.lbl_20, 0, 5);
            this.tlp_denominations.Controls.Add(this.lbl_10, 0, 6);
            this.tlp_denominations.Controls.Add(this.lbl_5x, 0, 7);
            this.tlp_denominations.Controls.Add(this.lbl_2x, 0, 8);
            this.tlp_denominations.Controls.Add(this.lbl_1, 0, 9);
            this.tlp_denominations.Controls.Add(this.tb_2000, 1, 0);
            this.tlp_denominations.Controls.Add(this.tb_500, 1, 1);
            this.tlp_denominations.Controls.Add(this.tb_200, 1, 2);
            this.tlp_denominations.Controls.Add(this.tb_100, 1, 3);
            this.tlp_denominations.Controls.Add(this.tb_50, 1, 4);
            this.tlp_denominations.Controls.Add(this.tb_20, 1, 5);
            this.tlp_denominations.Controls.Add(this.tb_10, 1, 6);
            this.tlp_denominations.Controls.Add(this.tb_5, 1, 7);
            this.tlp_denominations.Controls.Add(this.tb_2, 1, 8);
            this.tlp_denominations.Controls.Add(this.tb_1, 1, 9);
            this.tlp_denominations.Controls.Add(this.lbl_Rs2000, 2, 0);
            this.tlp_denominations.Controls.Add(this.lbl_Rs500, 2, 1);
            this.tlp_denominations.Controls.Add(this.lbl_Rs200, 2, 2);
            this.tlp_denominations.Controls.Add(this.lbl_Rs100, 2, 3);
            this.tlp_denominations.Controls.Add(this.lbl_Rs50, 2, 4);
            this.tlp_denominations.Controls.Add(this.lbl_Rs20, 2, 5);
            this.tlp_denominations.Controls.Add(this.lbl_Rs10, 2, 6);
            this.tlp_denominations.Controls.Add(this.lbl_Rs5, 2, 7);
            this.tlp_denominations.Controls.Add(this.lbl_Rs2, 2, 8);
            this.tlp_denominations.Controls.Add(this.lbl_Rs1, 2, 9);
            this.tlp_denominations.Location = new System.Drawing.Point(426, 80);
            this.tlp_denominations.Name = "tlp_denominations";
            this.tlp_denominations.RowCount = 10;
            this.tlp_denominations.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlp_denominations.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlp_denominations.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlp_denominations.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlp_denominations.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlp_denominations.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlp_denominations.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlp_denominations.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlp_denominations.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlp_denominations.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlp_denominations.Size = new System.Drawing.Size(346, 293);
            this.tlp_denominations.TabIndex = 0;
            // 
            // lbl_2000
            // 
            this.lbl_2000.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl_2000.AutoSize = true;
            this.lbl_2000.Location = new System.Drawing.Point(3, 4);
            this.lbl_2000.Name = "lbl_2000";
            this.lbl_2000.Size = new System.Drawing.Size(48, 20);
            this.lbl_2000.TabIndex = 0;
            this.lbl_2000.Text = "2000x";
            // 
            // lbl_500
            // 
            this.lbl_500.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl_500.AutoSize = true;
            this.lbl_500.Location = new System.Drawing.Point(11, 33);
            this.lbl_500.Name = "lbl_500";
            this.lbl_500.Size = new System.Drawing.Size(40, 20);
            this.lbl_500.TabIndex = 1;
            this.lbl_500.Text = "500x";
            // 
            // lbl_200
            // 
            this.lbl_200.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl_200.AutoSize = true;
            this.lbl_200.Location = new System.Drawing.Point(11, 62);
            this.lbl_200.Name = "lbl_200";
            this.lbl_200.Size = new System.Drawing.Size(40, 20);
            this.lbl_200.TabIndex = 2;
            this.lbl_200.Text = "200x";
            // 
            // lbl_100
            // 
            this.lbl_100.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl_100.AutoSize = true;
            this.lbl_100.Location = new System.Drawing.Point(11, 91);
            this.lbl_100.Name = "lbl_100";
            this.lbl_100.Size = new System.Drawing.Size(40, 20);
            this.lbl_100.TabIndex = 3;
            this.lbl_100.Text = "100x";
            // 
            // lbl_50
            // 
            this.lbl_50.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl_50.AutoSize = true;
            this.lbl_50.Location = new System.Drawing.Point(19, 120);
            this.lbl_50.Name = "lbl_50";
            this.lbl_50.Size = new System.Drawing.Size(32, 20);
            this.lbl_50.TabIndex = 4;
            this.lbl_50.Text = "50x";
            // 
            // lbl_20
            // 
            this.lbl_20.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl_20.AutoSize = true;
            this.lbl_20.Location = new System.Drawing.Point(19, 149);
            this.lbl_20.Name = "lbl_20";
            this.lbl_20.Size = new System.Drawing.Size(32, 20);
            this.lbl_20.TabIndex = 5;
            this.lbl_20.Text = "20x";
            // 
            // lbl_10
            // 
            this.lbl_10.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl_10.AutoSize = true;
            this.lbl_10.Location = new System.Drawing.Point(19, 178);
            this.lbl_10.Name = "lbl_10";
            this.lbl_10.Size = new System.Drawing.Size(32, 20);
            this.lbl_10.TabIndex = 6;
            this.lbl_10.Text = "10x";
            // 
            // lbl_5x
            // 
            this.lbl_5x.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl_5x.AutoSize = true;
            this.lbl_5x.Location = new System.Drawing.Point(27, 207);
            this.lbl_5x.Name = "lbl_5x";
            this.lbl_5x.Size = new System.Drawing.Size(24, 20);
            this.lbl_5x.TabIndex = 7;
            this.lbl_5x.Text = "5x";
            // 
            // lbl_2x
            // 
            this.lbl_2x.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl_2x.AutoSize = true;
            this.lbl_2x.Location = new System.Drawing.Point(27, 236);
            this.lbl_2x.Name = "lbl_2x";
            this.lbl_2x.Size = new System.Drawing.Size(24, 20);
            this.lbl_2x.TabIndex = 8;
            this.lbl_2x.Text = "2x";
            // 
            // lbl_1
            // 
            this.lbl_1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl_1.AutoSize = true;
            this.lbl_1.Location = new System.Drawing.Point(27, 267);
            this.lbl_1.Name = "lbl_1";
            this.lbl_1.Size = new System.Drawing.Size(24, 20);
            this.lbl_1.TabIndex = 9;
            this.lbl_1.Text = "1x";
            // 
            // tb_2000
            // 
            this.tb_2000.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tb_2000.Location = new System.Drawing.Point(57, 3);
            this.tb_2000.Name = "tb_2000";
            this.tb_2000.Size = new System.Drawing.Size(125, 27);
            this.tb_2000.TabIndex = 10;
            this.tb_2000.Text = "0";
            this.tb_2000.TextChanged += new System.EventHandler(this.tb_2000_TextChanged);
            // 
            // tb_500
            // 
            this.tb_500.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tb_500.Location = new System.Drawing.Point(57, 32);
            this.tb_500.Name = "tb_500";
            this.tb_500.Size = new System.Drawing.Size(125, 27);
            this.tb_500.TabIndex = 11;
            this.tb_500.Text = "0";
            this.tb_500.TextChanged += new System.EventHandler(this.tb_500_TextChanged);
            // 
            // tb_200
            // 
            this.tb_200.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tb_200.Location = new System.Drawing.Point(57, 61);
            this.tb_200.Name = "tb_200";
            this.tb_200.Size = new System.Drawing.Size(125, 27);
            this.tb_200.TabIndex = 12;
            this.tb_200.Text = "0";
            this.tb_200.TextChanged += new System.EventHandler(this.tb_200_TextChanged);
            // 
            // tb_100
            // 
            this.tb_100.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tb_100.Location = new System.Drawing.Point(57, 90);
            this.tb_100.Name = "tb_100";
            this.tb_100.Size = new System.Drawing.Size(125, 27);
            this.tb_100.TabIndex = 13;
            this.tb_100.Text = "0";
            this.tb_100.TextChanged += new System.EventHandler(this.tb_100_TextChanged);
            // 
            // tb_50
            // 
            this.tb_50.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tb_50.Location = new System.Drawing.Point(57, 119);
            this.tb_50.Name = "tb_50";
            this.tb_50.Size = new System.Drawing.Size(125, 27);
            this.tb_50.TabIndex = 14;
            this.tb_50.Text = "0";
            this.tb_50.TextChanged += new System.EventHandler(this.tb_50_TextChanged);
            // 
            // tb_20
            // 
            this.tb_20.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tb_20.Location = new System.Drawing.Point(57, 148);
            this.tb_20.Name = "tb_20";
            this.tb_20.Size = new System.Drawing.Size(125, 27);
            this.tb_20.TabIndex = 15;
            this.tb_20.Text = "0";
            this.tb_20.TextChanged += new System.EventHandler(this.tb_20_TextChanged);
            // 
            // tb_10
            // 
            this.tb_10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tb_10.Location = new System.Drawing.Point(57, 177);
            this.tb_10.Name = "tb_10";
            this.tb_10.Size = new System.Drawing.Size(125, 27);
            this.tb_10.TabIndex = 16;
            this.tb_10.Text = "0";
            this.tb_10.TextChanged += new System.EventHandler(this.tb_10_TextChanged);
            // 
            // tb_5
            // 
            this.tb_5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tb_5.Location = new System.Drawing.Point(57, 206);
            this.tb_5.Name = "tb_5";
            this.tb_5.Size = new System.Drawing.Size(125, 27);
            this.tb_5.TabIndex = 17;
            this.tb_5.Text = "0";
            this.tb_5.TextChanged += new System.EventHandler(this.tb_5_TextChanged);
            // 
            // tb_2
            // 
            this.tb_2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tb_2.Location = new System.Drawing.Point(57, 235);
            this.tb_2.Name = "tb_2";
            this.tb_2.Size = new System.Drawing.Size(125, 27);
            this.tb_2.TabIndex = 18;
            this.tb_2.Text = "0";
            this.tb_2.TextChanged += new System.EventHandler(this.tb_2_TextChanged);
            // 
            // tb_1
            // 
            this.tb_1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tb_1.Location = new System.Drawing.Point(57, 264);
            this.tb_1.Name = "tb_1";
            this.tb_1.Size = new System.Drawing.Size(125, 27);
            this.tb_1.TabIndex = 19;
            this.tb_1.Text = "0";
            this.tb_1.TextChanged += new System.EventHandler(this.tb_1_TextChanged);
            // 
            // lbl_Rs2000
            // 
            this.lbl_Rs2000.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbl_Rs2000.AutoSize = true;
            this.lbl_Rs2000.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_Rs2000.Location = new System.Drawing.Point(188, 4);
            this.lbl_Rs2000.Name = "lbl_Rs2000";
            this.lbl_Rs2000.Size = new System.Drawing.Size(18, 20);
            this.lbl_Rs2000.TabIndex = 20;
            this.lbl_Rs2000.Text = "0";
            // 
            // lbl_Rs500
            // 
            this.lbl_Rs500.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbl_Rs500.AutoSize = true;
            this.lbl_Rs500.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_Rs500.Location = new System.Drawing.Point(188, 33);
            this.lbl_Rs500.Name = "lbl_Rs500";
            this.lbl_Rs500.Size = new System.Drawing.Size(18, 20);
            this.lbl_Rs500.TabIndex = 21;
            this.lbl_Rs500.Text = "0";
            // 
            // lbl_Rs200
            // 
            this.lbl_Rs200.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbl_Rs200.AutoSize = true;
            this.lbl_Rs200.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_Rs200.Location = new System.Drawing.Point(188, 62);
            this.lbl_Rs200.Name = "lbl_Rs200";
            this.lbl_Rs200.Size = new System.Drawing.Size(18, 20);
            this.lbl_Rs200.TabIndex = 22;
            this.lbl_Rs200.Text = "0";
            // 
            // lbl_Rs100
            // 
            this.lbl_Rs100.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbl_Rs100.AutoSize = true;
            this.lbl_Rs100.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_Rs100.Location = new System.Drawing.Point(188, 91);
            this.lbl_Rs100.Name = "lbl_Rs100";
            this.lbl_Rs100.Size = new System.Drawing.Size(18, 20);
            this.lbl_Rs100.TabIndex = 23;
            this.lbl_Rs100.Text = "0";
            // 
            // lbl_Rs50
            // 
            this.lbl_Rs50.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbl_Rs50.AutoSize = true;
            this.lbl_Rs50.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_Rs50.Location = new System.Drawing.Point(188, 120);
            this.lbl_Rs50.Name = "lbl_Rs50";
            this.lbl_Rs50.Size = new System.Drawing.Size(18, 20);
            this.lbl_Rs50.TabIndex = 24;
            this.lbl_Rs50.Text = "0";
            // 
            // lbl_Rs20
            // 
            this.lbl_Rs20.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbl_Rs20.AutoSize = true;
            this.lbl_Rs20.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_Rs20.Location = new System.Drawing.Point(188, 149);
            this.lbl_Rs20.Name = "lbl_Rs20";
            this.lbl_Rs20.Size = new System.Drawing.Size(18, 20);
            this.lbl_Rs20.TabIndex = 25;
            this.lbl_Rs20.Text = "0";
            // 
            // lbl_Rs10
            // 
            this.lbl_Rs10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbl_Rs10.AutoSize = true;
            this.lbl_Rs10.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_Rs10.Location = new System.Drawing.Point(188, 178);
            this.lbl_Rs10.Name = "lbl_Rs10";
            this.lbl_Rs10.Size = new System.Drawing.Size(18, 20);
            this.lbl_Rs10.TabIndex = 26;
            this.lbl_Rs10.Text = "0";
            // 
            // lbl_Rs5
            // 
            this.lbl_Rs5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbl_Rs5.AutoSize = true;
            this.lbl_Rs5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_Rs5.Location = new System.Drawing.Point(188, 207);
            this.lbl_Rs5.Name = "lbl_Rs5";
            this.lbl_Rs5.Size = new System.Drawing.Size(18, 20);
            this.lbl_Rs5.TabIndex = 27;
            this.lbl_Rs5.Text = "0";
            // 
            // lbl_Rs2
            // 
            this.lbl_Rs2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbl_Rs2.AutoSize = true;
            this.lbl_Rs2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_Rs2.Location = new System.Drawing.Point(188, 236);
            this.lbl_Rs2.Name = "lbl_Rs2";
            this.lbl_Rs2.Size = new System.Drawing.Size(18, 20);
            this.lbl_Rs2.TabIndex = 28;
            this.lbl_Rs2.Text = "0";
            // 
            // lbl_Rs1
            // 
            this.lbl_Rs1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbl_Rs1.AutoSize = true;
            this.lbl_Rs1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_Rs1.Location = new System.Drawing.Point(188, 267);
            this.lbl_Rs1.Name = "lbl_Rs1";
            this.lbl_Rs1.Size = new System.Drawing.Size(18, 20);
            this.lbl_Rs1.TabIndex = 29;
            this.lbl_Rs1.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 112);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "User:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 20);
            this.label2.TabIndex = 7;
            this.label2.Text = "Date:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(496, 385);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(442, 385);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "Total:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(286, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(94, 29);
            this.button1.TabIndex = 4;
            this.button1.Text = "Get Denom";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btn_getDenom_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(560, 435);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(94, 29);
            this.button2.TabIndex = 3;
            this.button2.Text = "Submit";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.btn_dnm_submit_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(426, 435);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(94, 29);
            this.button3.TabIndex = 2;
            this.button3.Text = "Edit";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(10, 12);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(250, 27);
            this.dateTimePicker1.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.Size = new System.Drawing.Size(200, 100);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "2000x";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 20);
            this.label6.TabIndex = 1;
            this.label6.Text = "500x";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.button3);
            this.tabPage1.Controls.Add(this.dateTimePicker1);
            this.tabPage1.Location = new System.Drawing.Point(0, 0);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(200, 100);
            this.tabPage1.TabIndex = 0;
            // 
            // D_COMM_MAIN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(925, 523);
            this.Controls.Add(this.tc_main);
            this.IsMdiContainer = true;
            this.Name = "D_COMM_MAIN";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "D_COMM";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.C_COMM_MAIN_Load);
            this.tc_main.ResumeLayout(false);
            this.tp_denominations.ResumeLayout(false);
            this.tp_denominations.PerformLayout();
            this.tlp_denominations.ResumeLayout(false);
            this.tlp_denominations.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.TabControl tc_main;
        private System.Windows.Forms.TabPage tp_summary;
        private System.Windows.Forms.TabPage tp_denominations;
        private System.Windows.Forms.TableLayoutPanel tlp_denominations;
        private System.Windows.Forms.Label lbl_2000;
        private System.Windows.Forms.Label lbl_500;
        private System.Windows.Forms.Label lbl_200;
        private System.Windows.Forms.Label lbl_100;
        private System.Windows.Forms.Label lbl_50;
        private System.Windows.Forms.Label lbl_20;
        private System.Windows.Forms.Label lbl_10;
        private System.Windows.Forms.Label lbl_5x;
        private System.Windows.Forms.Label lbl_2x;
        private System.Windows.Forms.Label lbl_1;
        private System.Windows.Forms.TextBox tb_2000;
        private System.Windows.Forms.TextBox tb_500;
        private System.Windows.Forms.TextBox tb_200;
        private System.Windows.Forms.TextBox tb_100;
        private System.Windows.Forms.TextBox tb_50;
        private System.Windows.Forms.TextBox tb_20;
        private System.Windows.Forms.TextBox tb_10;
        private System.Windows.Forms.TextBox tb_5;
        private System.Windows.Forms.TextBox tb_2;
        private System.Windows.Forms.TextBox tb_1;
        private System.Windows.Forms.DateTimePicker dtp_denominations;
        private System.Windows.Forms.Button btn_dnm_submit;
        private System.Windows.Forms.Button btn_dnm_edit;
        private System.Windows.Forms.Button btn_getDenom;
        private System.Windows.Forms.Label lbl_Rs2000;
        private System.Windows.Forms.Label lbl_Rs500;
        private System.Windows.Forms.Label lbl_Rs200;
        private System.Windows.Forms.Label lbl_Rs100;
        private System.Windows.Forms.Label lbl_Rs50;
        private System.Windows.Forms.Label lbl_Rs20;
        private System.Windows.Forms.Label lbl_Rs10;
        private System.Windows.Forms.Label lbl_Rs5;
        private System.Windows.Forms.Label lbl_Rs2;
        private System.Windows.Forms.Label lbl_Rs1;
        private System.Windows.Forms.Label lbl_Total;
        private System.Windows.Forms.Label lbl_RsTotal;
        private System.Windows.Forms.Label lbl_Date;
        private System.Windows.Forms.Label lbl_User;
        private System.Windows.Forms.Label lbl_lstUpd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label lbl_lstUpdVal;
        private System.Windows.Forms.Label lbl_UserValue;
        private System.Windows.Forms.Label lbl_DateValue;
    }
}

