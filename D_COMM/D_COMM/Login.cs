﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BusinessLogic;
using Entities;


namespace D_COMM
{
    public partial class Login : Form
    {     
        private readonly D_COMM_BLL bll = new D_COMM_BLL();
        public Login()
        {
            InitializeComponent();
        }

        private void btn_submit_Click(object sender, EventArgs e)
        {
            bool knownUser = bll.VadidateUser();
            if(knownUser) {
                Close();
            }
            else
            {
                MessageBox.Show("Usename or Password is incorrect. Enter valid credentials.");
            }
        }

        private void tb_username_TextChanged(object sender, EventArgs e)
        {
            User.UserName = tb_username.Text;
        }

        private void tb_password_TextChanged(object sender, EventArgs e)
        {
            User.Password = tb_password.Text;
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            this.AcceptButton = btn_submit;
        }
    }
}
