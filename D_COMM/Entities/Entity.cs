﻿using System;

namespace Entities
{
    public class Entity
    {
    }

    public static class User
    {
        public static int UserId;
        public static string UserName;
        public static string Password;
        public static string Role;
        public static string LastUpdateDate;        
    }

    public class Denomination
    {
        private int _2000;
        private int _500;
        private int _200;
        private int _100;
        private int _50;
        private int _20;
        private int _10;
        private int _5;
        private int _2;
        private int _1;
        private string _date;
        private string _lastUpdateDate;
        private string _user;

        public int Rs2000
        {
            get { return _2000; }
            set { _2000 = value; }
        }
        public int Rs500
        {
            get { return _500; }
            set { _500 = value; }
        }
        public int Rs200
        {
            get { return _200; }
            set { _200 = value; }
        }
        public int Rs100
        {
            get { return _100; }
            set { _100 = value; }
        }
        public int Rs50
        {
            get { return _50; }
            set { _50 = value; }
        }
        public int Rs20
        {
            get { return _20; }
            set { _20 = value; }
        }
        public int Rs10
        {
            get { return _10; }
            set { _10 = value; }
        }
        public int Rs5
        {
            get { return _5; }
            set { _5 = value; }
        }
        public int Rs2
        {
            get { return _2; }
            set { _2 = value; }
        }
        public int Rs1
        {
            get { return _1; }
            set { _1 = value; }
        }
        public int RsTotal
        {
            get { return Rs1*1 + Rs2*2 + Rs5*5 + Rs10*10 + Rs20*20 + Rs50*50 + Rs100*100 + Rs200*200 + Rs500*500 + Rs2000*2000; }            
        }
        public string Date
        {
            get { return _date; }
            set { _date = value; }
        }
        public string LastUpdateDate
        {
            get { return _lastUpdateDate; }
            set { _lastUpdateDate = value; }
        }
        public string User
        {
            get { return _user; }
            set { _user = value; }
        }

        public int Total2000
        {
            get { return Rs2000 * 2000; }
        }
        public int Total500
        {
            get { return Rs500 * 500; }
        }
        public int Total200
        {
            get { return Rs200 * 200; }
        }
        public int Total100
        {
            get { return Rs100 * 100; }
        }
        public int Total50
        {
            get { return Rs50 * 50; }
        }
        public int Total20
        {
            get { return Rs20 * 20; }
        }
        public int Total10
        {
            get { return Rs10 * 10; }
        }
        public int Total5
        {
            get { return Rs5 * 5; }
        }
        public int Total2
        {
            get { return Rs2 * 2; }
        }
        public int Total1
        {
            get { return Rs1 * 1; }
        }

    }

}
