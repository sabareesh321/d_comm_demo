﻿using System;
using DataAccess;
using Entities;

namespace BusinessLogic
{
    public class D_COMM_BLL
    {
        private readonly D_COMM_DAL dal = new D_COMM_DAL();
        public bool VadidateUser()
        {
            int userId = dal.GetUser();
            if (userId > 0)
            {
                return true;
            }
            return false;
        }

        public void SubmitDenoms(Denomination denomination)
        {
            dal.InsertDenom(denomination);
        }

        public bool GetDenoms(Denomination denomination)
        {
            return dal.GetDenoms(denomination);
        }
        public bool EditDenom(Denomination denomination)
        {
            return dal.UpdateDenom(denomination);
        }
    }
    
}
