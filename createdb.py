import sqlite3
from sqlite3 import Error


def create_connection(db_file):
    """ create a database connection to a SQLite database """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print('1')
        print(e)

def create_table(conn, create_table_sql):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print('2')
        print(e)
    

if __name__ == '__main__':
    conn = create_connection(r"C:\ProgramData\D_COMM\dcommsqlite.db")
    table_users = """ create table if not exists USERS(
                            USER_ID INTEGER PRIMARY KEY AUTOINCREMENT,
                            USER_NAME TEXT NOT NULL UNIQUE,
                            PASSWORD TEXT NOT NULL,
                            ROLE TEXT DEFAULT 'user' NOT NULL,
                            LAST_UPDATE_DATE DATETIME DEFAULT (datetime('now','localtime')) NOT NULL
                            );
                            """
    table_denom = """ create table if not exists DENOMINATION(
                    DATE TEXT PRIMARY KEY,
                    ENTERED_BY TEXT NOT NULL,
                    RS_2000 INTEGER DEFAULT 0 NOT NULL,
                    RS_500 INTEGER DEFAULT 0 NOT NULL,
                    RS_200 INTEGER DEFAULT 0 NOT NULL,
                    RS_100 INTEGER DEFAULT 0 NOT NULL,
                    RS_50 INTEGER DEFAULT 0 NOT NULL,
                    RS_20 INTEGER DEFAULT 0 NOT NULL,
                    RS_10 INTEGER DEFAULT 0 NOT NULL,
                    RS_5 INTEGER DEFAULT 0 NOT NULL,
                    RS_2 INTEGER DEFAULT 0 NOT NULL,
                    RS_1 INTEGER DEFAULT 0 NOT NULL,
                    LAST_UPDATE_DATE DATETIME DEFAULT (datetime('now','localtime')) NOT NULL
                    );
                    """
    if conn is not None:      
        
        c = conn.cursor()
        #c.execute("DROP TABLE IF EXISTS DENOMINATION")
        #c.execute("DELETE FROM DENOMINATION")
        #create_table(conn, table_users)
        #c.execute('insert into users(USER_NAME,PASSWORD,ROLE)values(?,?,?)',('S','s','admin'))

        create_table(conn, table_denom)
        #c.execute('insert into DENOMINATION values(?,?,?,?,?,?,?,?,?,?,?,?,?)',('2021-03-25','ADMIN',1,1,1,1,1,1,1,1,1,1,'2021-03-26 10:39:05'))
        #c.execute("insert into DENOMINATION values ('2021-03-26','ADMIN',1,1,1,1,1,1,1,1,1,1,'2021-03-26 10:39:05')",)
        conn.commit()
        c.execute("SELECT * FROM DENOMINATION")
        x=c.fetchall()
        for i in x:
            print(i)
        
    
#{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12}
